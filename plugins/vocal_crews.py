from datetime import datetime
import logging
import random
import time

from disco.types.channel import Channel
from disco.gateway.events import MessageReactionAdd, ChannelCreate
from disco.types.guild import GuildMember
from disco.types.invite import Invite
from disco.types.message import MessageEmbed
from disco.bot.plugin import Plugin
from disco.api.http import APIException


class VocalCrewsPlugin(Plugin):
    known_guilds = {}
    crew_creators = set()
    invites = {}
    used_names = {}

    @Plugin.listen('GuildCreate')
    def on_guild_create(self, event):
        guild = event.guild
        if guild.id in self.known_guilds:
            return
        logging.info('Setuping voice channels for guild "{}" (#{})'.format(guild.name, guild.id))
        config_categories = [int(c) for c in self.config['categories']]
        categories = set(guild.channels).intersection(config_categories)
        self.known_guilds[guild.id] = categories
        for category_id in categories:
            category = guild.channels[category_id]
            logging.info('Setting category "{}" (#{}) as vocal crew category'.format(category.name, category.id))
            guild_channels = category.guild.channels.find(lambda c: c.is_voice and c.parent_id == category_id)
            for channel in guild_channels:
                if not channel.guild.voice_states.select_one(channel_id=channel.id):
                    logging.info('Deleting unknown voice channel "{}" (#{})'.format(channel.name, channel.id))
                    self.spawn(channel.delete)
                else:
                    logging.warning(
                        'Leaving non-empty unknown voice channel "{}" (#{})'.format(channel.name, channel.id)
                    )
            self.spawn(self.create_creator_channel, category)

    @Plugin.listen('VoiceStateUpdate')
    def on_voice_state_update(self, event):
        if event.state.channel_id in self.crew_creators:
            self.spawn(self.create_crew_channel, event.state.channel, event.state.user)
            self.spawn(self.create_creator_channel, event.state.channel.parent)
        self.spawn(self.clean_empty_channels, event.state.guild)
        self.spawn(self.update_invite_messages)

    @Plugin.listen('ChannelCreate')
    def on_channel_create(self, event: ChannelCreate):
        channel = event.channel
        guild_id = channel.guild_id
        new_channel_name = self.get_category_config(channel.parent, 'new_crew_name')
        if channel.parent_id in self.known_guilds[guild_id] and channel.name == new_channel_name:
            self.crew_creators.add(channel.id)
            try:
                self.allow_api_exception(channel.set_position, 10003, 1)
            except APIException:
                logging.warning("Creator channel has been deleted before being protected, retrying in 5 seconds…")
                self.crew_creators.remove(channel.id)
                self.spawn_later(5, self.create_creator_channel, channel)

    @Plugin.listen('ChannelDelete')
    def on_channel_delete(self, event):
        deleted_channel_id = event.channel.id
        parent_id = event.channel.parent_id
        if parent_id in self.crew_creators:
            self.crew_creators.remove(deleted_channel_id)
        if deleted_channel_id in self.invites:
            messages_to_delete = self.invites[deleted_channel_id]
            del self.invites[deleted_channel_id]
            for channel_id, message in messages_to_delete.items():
                self.spawn(
                    self.allow_api_exception,
                    self.client.api.channels_messages_delete,
                    10008,
                    channel_id,
                    message['message_id']
                )

    @Plugin.command('!i', '[msg:str...]')
    def on_invite_command(self, event, msg=None):
        self.spawn(self.allow_api_exception, event.msg.delete, 10008)
        managed_categories = [int(c) for c in self.config['categories']]
        voice_state = event.guild.voice_states.select_one(user=event.author)
        if voice_state:
            if voice_state.channel.parent.id not in managed_categories:
                return
            alert_channels = self.get_category_config(voice_state.channel.parent, 'alert_allowed_channels')
            if event.channel.id in alert_channels:
                self.spawn(self.send_new_alert, event.channel, voice_state.channel, event.member, msg)

    @Plugin.listen('MessageReactionAdd')
    def on_message_reaction_add(self, event: MessageReactionAdd):
        if event.user_id is self.state.me:
            return
        managed_categories = [int(c) for c in self.config['categories']]
        voice_state = event.guild.voice_states.find_one(
            lambda vs: vs.user == event.user_id and vs.channel.parent_id in managed_categories
        )
        if voice_state:
            delete_emoji = self.get_category_config(voice_state.channel.parent, 'alert_message_delete_emoji')
            if event.emoji.name != delete_emoji:
                return
            try:
                invite = self.invites[voice_state.channel_id][event.channel_id]
                if invite['message_id'] != event.message_id:
                    return
                del self.invites[voice_state.channel_id][event.channel_id]
                self.client.api.channels_messages_delete(event.channel_id, event.message_id)
            except KeyError:
                return

    def create_crew_channel(self, channel, user):
        self.crew_creators.remove(channel.id)
        crew_names = self.get_category_config(channel.parent, 'crew_names')
        crew_formatter = self.get_category_config(channel.parent, 'crew_formatter')
        if channel.parent.id not in self.used_names:
            self.used_names[channel.parent.id] = set()
        used_names = self.used_names[channel.parent.id]
        available_names = set(crew_names).difference(used_names)
        chosen_name = random.choice(list(available_names))
        used_names.add(chosen_name)
        if len(used_names) == len(crew_names):
            used_names.clear()
        new_channel_name = crew_formatter.format(chosen_name)
        log_msg = 'Creating Crew "{}" (#{}) (requested by {})'.format(
            new_channel_name,
            channel.id,
            str(user)
        )
        logging.info(log_msg)
        self.client.api.channels_modify(channel.id, name=new_channel_name, position=int(time.time()))
        log_channel = self.get_category_config(channel.parent, 'log_channel')
        if log_channel in channel.guild.channels:
            self.client.api.channels_messages_create(
                log_channel, "[{:%X}] {}".format(datetime.now(), log_msg)
            )
        return channel

    def create_creator_channel(self, category):
        channel_name = self.get_category_config(category, 'new_crew_name')
        channel_limit = self.get_category_config(category, 'crew_size')
        try:
            category.create_voice_channel(channel_name, user_limit=channel_limit)
        except ConnectionError:
            # The crew creation has failed, let's restart
            logging.warning("Network error, can't create channel, retrying in 5 seconds…")
            self.spawn_later(5, self.create_creator_channel, category)

    def clean_empty_channels(self, guild):
        if guild.id not in self.known_guilds:
            return
        categories = self.known_guilds[guild.id]
        guild_channels = guild.channels.find(
            lambda c: c.is_voice and c.id not in self.crew_creators and c.parent_id in categories
        )
        for channel in guild_channels:
            if not channel.guild.voice_states.select_one(channel_id=channel.id):
                logging.info('Deleting empty channel "{}" (#{})'.format(channel.name, channel.id))
                self.spawn(self.allow_api_exception, channel.delete, 10003)

    def generate_invite_embed(self, member: GuildMember, voice_channel: Channel, user_message: str):
        category = voice_channel.parent
        formatter = self.get_category_config(category, 'alert_message_formatter')
        footer = self.get_category_config(category, 'alert_message_footer')

        if user_message is not None:
            alert_message = self.get_category_config(category, 'alert_message_custom')
            formatted_msg = alert_message.format(user_msg=user_message)
        else:
            formatted_msg = self.get_category_config(category, 'alert_message_standard')

        join_msg = voice_channel.mention

        available_slots = self.get_available_slots(voice_channel)
        if available_slots == -1:
            available_slots_msg = ""
        elif available_slots == 0:
            join_msg = ""
            available_slots_msg = self.get_category_config(category, 'alert_message_no_slot')
        elif available_slots == 1:
            available_slots_msg = self.get_category_config(category, 'alert_message_one_slot')
        else:
            available_slots_template = self.get_category_config(category, 'alert_message_several_slots')
            available_slots_msg = available_slots_template.format(slots=available_slots)

        crew_members = self.get_connected_users(voice_channel)
        crew_members_text = ", ".join(["<@!{}>".format(m) for m in crew_members])

        embed = MessageEmbed()
        embed.set_author(name=member.name, icon_url=member.user.avatar_url)
        embed.description = formatter.format(
            msg=formatted_msg,
            join=join_msg,
            available_slots=available_slots_msg,
            crew_members=crew_members_text
        )
        embed.set_footer(text=footer)
        return embed

    def update_invite_messages(self):
        for voice_channel_id, invites in dict(self.invites).items():
            for alert_channel_id, message in invites.items():
                voice_channel = self.state.channels[voice_channel_id] or None
                if not voice_channel:
                    continue
                current_connected_users = self.get_connected_users(voice_channel)
                if len(current_connected_users) > 0 and current_connected_users != message['connected_users']:
                    embed = self.generate_invite_embed(
                        message['member'],
                        voice_channel,
                        message['user_message']
                    )
                    self.client.api.channels_messages_modify(alert_channel_id, message['message_id'], embed=embed)
                    message['connected_users'] = current_connected_users

                    if self.get_available_slots(voice_channel) == 0:
                        reaction_emoji = self.get_category_config(voice_channel.parent, 'alert_message_delete_emoji')
                        self.client.api.channels_messages_reactions_create(
                            alert_channel_id,
                            message['message_id'],
                            reaction_emoji
                        )

    def send_new_alert(self, alert_channel, voice_channel, member, user_message):
        if voice_channel.id not in voice_channel.guild.channels:
            return
        if voice_channel.id not in self.invites:
            self.invites[voice_channel.id] = {}
        if alert_channel.id in self.invites[voice_channel.id]:
            message_id = self.invites[voice_channel.id][alert_channel.id]['message_id']
            try:
                self.client.api.channels_messages_delete(alert_channel.id, message_id)
            except APIException as e:
                if e.code != 10008:
                    raise e

        if user_message is not None:
            log_msg = "Sending invite requested by {} for channel {} (#{}), with custom message:\n```{}```".format(
                str(member), alert_channel.name, alert_channel.id, user_message
            )
        else:
            log_msg = "Sending invite requested by {} for channel {} (#{}) without custom message".format(
                str(member), alert_channel.name, alert_channel.id
            )
        logging.info(log_msg)

        embed = self.generate_invite_embed(member, voice_channel, user_message)
        connected_users = self.get_connected_users(voice_channel)
        invite_msg = alert_channel.send_message(embed=embed)
        self.invites[voice_channel.id][alert_channel.id] = {
            'message_id': invite_msg.id,
            'member': member,
            'user_message': user_message,
            'connected_users': connected_users
        }

        log_channel = self.get_category_config(voice_channel.parent, 'log_channel')
        if log_channel in alert_channel.guild.channels:
            self.client.api.channels_messages_create(
                log_channel, "[{:%X}] {}".format(datetime.now(), log_msg)
            )

    def get_category_config(self, category: Channel, param):
        category_config = self.config['categories'].get(str(category.id), {})
        config = category_config.get(param, self.config[param])
        return config


    @staticmethod
    def allow_api_exception(method, code, *args, **kwargs):
        try:
            method(*args, **kwargs)
        except APIException as e:
            if e.code != code:
                raise e

    @staticmethod
    def get_connected_users(voice_channel: Channel):
        voice_states = voice_channel.guild.voice_states.select(channel_id=voice_channel.id)
        return set([vs.user_id for vs in voice_states])

    @classmethod
    def get_available_slots(cls, voice_channel: Channel):
        channel_user_limit = voice_channel.user_limit or 0
        if channel_user_limit == 0:
            return -1
        users = cls.get_connected_users(voice_channel)
        connected_users = len(users)
        return channel_user_limit - connected_users
