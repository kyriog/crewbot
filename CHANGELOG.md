# v1.0

* Managing a category for creating on-demand voice channels
* Allowing to manage multiple category
* Allowing to set a user limit per channel
* Configurable Crew creator name, crews names per category

# v1.5

* Crew creators channels are now moved on top of the category
* Trying to remove a bug which may keep alive empty channels
* !i command, which allows users to send an invitation for the created channel
* Text channels in managed category are no longer deleted
* Adding a configurable log channel for moderation infos about created channels and invites sent

# v1.5.1

* Fixing the time log for Python < 3.6

# v1.5.2

* Adding crew creator to known creators before moving it

# v1.6.0

* Discord tasks are now mostly done using multitasking
* Moving to Disco fork which prevent voice_states cache corruption

# v2.0.0

* !i invitations are now using Discord embeds
* !i messages now displays the remaining available slots
* !i messages can displays current crew members
* Crew members can now remove !i messages using a message reaction
* Bot automatically displays a deletion hint when the crew is full
* Disco library has been updated to the latest version (0.0.13rc2)

# v2.0.1

* Retrying creating creator crew if request got a ConnectionError
* Recreating creator crew if channel has been deleted too early
* Removing log error if a user reacts to a message while he's connected to a channel without category
* Preventing invites update interruption if a new invite is generated at the same time

# v3.0.0

* Crew Creators are now managed using the Channel Create event to prevent creators destruction
* Deprecating "enabled" configuration
* Refactoring code for better performance and code readability
* Adding `__init__.py` to respect Python coding standards

# v3.1.0

* Replacing invite link by room mention
